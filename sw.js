

//imports

importScripts('js/sw-utils.js');


const STATIC_CACHE    = 'static-v2';
const DINAMIC_CACHE   = 'dinamic-v1';
const INMUTABLE_CACHE = 'inmutable-v1';

//Agrego elementos del HTML que hize yo como el index, la hoja de estilo
// las imagenes que tengo guardadas de manera local en el proyecto
const APP_SHELL = [
    // '/',
    'index.html',
    'css/style.css',
    'img/favicon.ico',
    'img/avatars/hulk.jpg',
    'img/avatars/ironman.jpg',
    'img/avatars/spiderman.jpg',
    'img/avatars/thor.jpg',
    'img/avatars/wolverine.jpg',
    'js/app.js',
    'js/sw-utils.js'
];

// Agrego todos los archivos y librerias que agregue de terceros como Google Font, Animate CSS, fontAwesome
const APP_SHELL_INMUTABLE = [
    'https://fonts.googleapis.com/css?family=Quicksand:300,400',
    'https://fonts.googleapis.com/css?family=Lato:400,300',
    'https://use.fontawesome.com/releases/v5.3.1/css/all.css',
    'css/animate.css',
    'js/libs/jquery.js'
];

//Instalacion

self.addEventListener('install', e => {

    const cacheStatic = caches.open( STATIC_CACHE).then(cache =>
        cache.addAll(APP_SHELL ));
    
        const cacheInmutable = caches.open( INMUTABLE_CACHE).then(cache =>
        cache.addAll(APP_SHELL_INMUTABLE ));


    e.waitUntil( Promise.all([cacheStatic, cacheInmutable]));

});

// Activacion 

self.addEventListener('activate', e => {
    
    const respuesta = caches.keys().then(keys => {

        keys.forEach(key => {

            //status-v4
            if(key != STATIC_CACHE && key.includes('staticCahe')){
                return caches.delete(key);
            }
        });
    });
    e.waitUntil(respuesta);
});

//Peticion 

self.addEventListener('fetch', e => {

    const respuesta = caches.match(e.request).then( res => {
      

        if( res ){
            return res;
        }else{
           
            return fetch(e.request).then( newRes => {

                return actualizarCacheDinamico( DINAMIC_CACHE, e.request, newRes );

            })

        }
    });

    e.respondWith( respuesta );

});











